const express = require('express');

const emojis = require('./emojis');

const usuarios = require('./usuarios');

const router = express.Router();

router.get('/', (req, res) => {
    res.json({
        message: 'API - 👋🌎🌍🌏',
    });
});

router.use('/emojis', emojis);
router.use('/users', usuarios);

module.exports = router;
