const express = require('express');
const joi = require('@hapi/joi');
const monk = require('monk');

const router = express.Router();

const db = monk('root:example@localhost:27017/users?authSource=admin');
const usuarios = db.get('users');

const schema = joi.object({
    nombre: joi.string().trim().required(),
    apellido: joi.string().trim().required(),
    identification: joi.number().required(),
});

router.get('/', async (req, res, next) => {
    try {
        const users = await usuarios.find({});
        res.json(users);
    } catch (error) {
        next(error);
    }
});

router.post('/', async (req, res, next) => {
    try {
        const usuario = await schema.validateAsync(req.body);
        const insert = await usuarios.insert(usuario);
        res.json(insert);
    } catch (error) {
        next(error);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        const { id: _id } = req.params;
        const user = await usuarios.findOne({ _id });
        if (!user) return next();
        res.json(user);
    } catch (error) {
        next(error);
    }
});

router.put('/:id', async (req, res, next) => {
    try {
        const { id: _id } = req.params;
        let user = await usuarios.findOne({ _id });
        if (!user) return next();
        user = { ...user, ...req.body };
        await usuarios.findOneAndUpdate({ _id }, { $set: user });
        res.json(user);
    } catch (error) {
        next(error);
    }
});

router.delete('/:id', async (req, res, next) => {
    try {
        const { id: _id } = req.params;
        let user = await usuarios.findOne({ _id });
        if (!user) return next();
        await usuarios.findOneAndDelete({ _id });
        res.json({ message: 'Usuario eliminado' });
    } catch (error) {
        next(error);
    }
});

module.exports = router;
